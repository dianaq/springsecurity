

FROM openjdk:8u201-jdk-alpine3.9

MAINTAINER Alex <953222906@qq.com>

ENV LANG=C.UTF-8 LC_ALL=C.UTF-8

VOLUME /tmp

#包名
ADD target/security-1.0.jar app.jar

ENTRYPOINT ["java","-jar","/app.jar"]

EXPOSE 8989

