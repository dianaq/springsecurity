package com.dian;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * Created by dian on 2020/8/7 16:02
 */
public class Encoder {
    public static void main(String[] args) {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        String dian = bCryptPasswordEncoder.encode("dianay");
        System.out.println(dian);

        int a = 123;
        Integer b = 123;
        Integer c = 130;

        System.out.println(a == b ? "相等" : "不相等");
        System.out.println(a == c ? "相等" : "不相等");


    }
}
