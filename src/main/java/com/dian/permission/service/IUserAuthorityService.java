package com.dian.permission.service;

import com.dian.permission.pojo.UserAuthority;
import com.dian.permission.pojo.UserInfo;

import java.util.List;

/**
 * Created by dian on 2020/8/5 16:19
 */
public interface IUserAuthorityService {

    List<UserAuthority> getUserAuthorityByUserId(String userId);
}
