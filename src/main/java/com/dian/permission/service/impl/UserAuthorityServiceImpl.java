package com.dian.permission.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.dian.permission.dao.UserAuthorityMapper;
import com.dian.permission.dao.UserInfoMapper;
import com.dian.permission.pojo.UserAuthority;
import com.dian.permission.pojo.UserInfo;
import com.dian.permission.service.IUserAuthorityService;
import com.dian.permission.service.IUserInfoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by dian on 2020/8/5 16:20
 */
@Service
public class UserAuthorityServiceImpl implements IUserAuthorityService {

    @Resource
    UserAuthorityMapper userAuthorityMapper;

    @Override
    public List<UserAuthority> getUserAuthorityByUserId(String userId) {
        return userAuthorityMapper.getUserAuthorityByUserId(userId);
    }
}
