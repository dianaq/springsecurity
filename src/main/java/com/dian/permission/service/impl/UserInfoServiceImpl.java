package com.dian.permission.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.dian.permission.dao.UserInfoMapper;
import com.dian.permission.pojo.UserInfo;
import com.dian.permission.service.IUserInfoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by dian on 2020/8/5 16:20
 */
@Service
public class UserInfoServiceImpl implements IUserInfoService {

    @Resource
    UserInfoMapper userInfoMapper;

    @Override
    public UserInfo getByUserName(String userName) {
        QueryWrapper<UserInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.like("username", userName);
        return userInfoMapper.selectOne(queryWrapper);
    }
}
