package com.dian.permission.service;

import com.dian.permission.pojo.UserInfo;

/**
 * Created by dian on 2020/8/5 16:19
 */
public interface IUserInfoService {

    UserInfo getByUserName(String userName);
}
