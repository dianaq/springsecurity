package com.dian.permission.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dian.permission.pojo.UserAuthority;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Created by dian on 2020/8/5 16:18
 */
public interface UserAuthorityMapper extends BaseMapper<UserAuthority> {

    @Select("select distinct a.* from user_authority a inner join user_role_authorith ra on a.id = ra.authority_id inner join user_info_role ir on ir.role_id = ra.role_id where ir.info_id = #{userId}")
    List<UserAuthority> getUserAuthorityByUserId(@Param("userId") String userId);
}
