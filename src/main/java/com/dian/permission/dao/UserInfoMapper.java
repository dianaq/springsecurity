package com.dian.permission.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dian.permission.pojo.UserInfo;

/**
 * Created by dian on 2020/8/5 16:18
 */
public interface UserInfoMapper extends BaseMapper<UserInfo> {
}
