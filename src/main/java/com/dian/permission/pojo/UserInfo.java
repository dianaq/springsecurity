package com.dian.permission.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * 用户实体类
 * Created by dian on 2020/8/5 16:15
 */
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@TableName("user_info")
public class UserInfo implements Serializable {

    @TableId(value = "id",type = IdType.INPUT)
    private String id;

    private String username;

    private String password;

    private String phone;

    private String sex;

    private String state;

    private Boolean enabled;

    private Date createTime;

    private Date updateTime;


    /** 状态 */
    public static enum State{
        //正常
        USABLE("1"),
        //废除
        Disabled("2"),
        //锁定
        LOCK("3");

        public String type;
        State(String type) {
            this.type = type;
        }
    }

    @TableField(exist = false)
    private List<UserRole> listRole;

}
