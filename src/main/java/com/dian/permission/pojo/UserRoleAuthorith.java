package com.dian.permission.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;

import java.io.Serializable;

/**
 * Created by dian on 2020/8/7 10:12
 */
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@TableName("user_role_authorith")
public class UserRoleAuthorith implements Serializable {

    @TableId(value = "id",type = IdType.INPUT)
    private String id;

    private String roleId;

    private String authorityId;


}
