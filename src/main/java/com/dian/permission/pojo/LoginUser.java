package com.dian.permission.pojo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.security.Permission;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by dian on 2020/8/7 12:20
 */

public class LoginUser extends UserInfo implements UserDetails{

    private List<UserAuthority> permissions;

    private String token;

    private Long loginTime;

    private Long expireTime;

    public List<UserAuthority> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<UserAuthority> permissions) {
        this.permissions = permissions;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Long getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Long loginTime) {
        this.loginTime = loginTime;
    }

    public Long getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(Long expireTime) {
        this.expireTime = expireTime;
    }

    @Override
    @JsonIgnore
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return permissions.parallelStream().filter(p-> StringUtils.isNotEmpty(p.getAuthorityKey())).map(p ->new SimpleGrantedAuthority(p.getAuthorityKey())).collect(Collectors.toList());
    }



    /** 是否未过期 */
    @Override
    @JsonIgnore
    public boolean isAccountNonExpired() {
        return true;
    }

    /** 是否未锁定 */
    @Override
    @JsonIgnore
    public boolean isAccountNonLocked() {
        return getState() != State.LOCK.type;
    }

    /** 密码是否未过期 */
    @Override
    @JsonIgnore
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /** 是否激活 */
    @Override
    @JsonIgnore
    public boolean isEnabled() {
        return getEnabled();
    }
}
