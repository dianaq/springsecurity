package com.dian.common.pojo;

import java.util.HashMap;

/**
 * 与前端交互的对象
 * Created by dian on 2020/8/6 17:39
 */
public class RestResult extends HashMap<String,Object> {

    static final String CODE_TAG = "code";

    static final String MSG_TAG = "msg";

    static final String DATA_TAG = "data";

    public RestResult() {
    }

    public RestResult(int code, String msg) {
        super.put(CODE_TAG,code);
        super.put(MSG_TAG,msg);
    }

    public RestResult(int code, String msg,Object data) {
        super.put(CODE_TAG,code);
        super.put(MSG_TAG,msg);
        super.put(DATA_TAG,data);
    }
}
