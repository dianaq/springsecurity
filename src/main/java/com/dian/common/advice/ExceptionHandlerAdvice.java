package com.dian.common.advice;

import com.dian.common.pojo.RestResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * controller 全局异常处理
 * Created by dian on 2020/8/7 15:25
 */
@RestControllerAdvice
@Slf4j
public class ExceptionHandlerAdvice {

    @ExceptionHandler({IllegalArgumentException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public RestResult badEuquestException(IllegalArgumentException e){
        return new RestResult(HttpStatus.BAD_REQUEST.value(),e.getMessage());
    }
}
