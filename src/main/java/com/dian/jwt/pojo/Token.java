package com.dian.jwt.pojo;

import lombok.*;

import java.io.Serializable;

/**
 * Created by dian on 2020/8/7 18:14
 */
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Token implements Serializable {

    private String token;

    private Long loginTime;


}
