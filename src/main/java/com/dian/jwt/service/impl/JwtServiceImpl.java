package com.dian.jwt.service.impl;

import com.dian.jwt.pojo.Token;
import com.dian.jwt.service.ITokenService;
import com.dian.permission.pojo.LoginUser;
import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jose.crypto.MACVerifier;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jwt.proc.BadJWTException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.rsa.crypto.KeyStoreKeyFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.security.KeyPair;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.text.ParseException;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Created by dian on 2020/8/10 9:54
 */
@Slf4j
@Service
@Primary
public class JwtServiceImpl implements ITokenService {

    @Value("${token.expire.seconds}")
    private Integer expireSeconds;

    @Value("${token.jwt.secret}")
    private String secret;

    @Value("${redis.spring.security.key}")
    private String redisKey;

    @Resource
    private RedisTemplate<String, LoginUser> redisTemplate;


    @Override
    public Token getToken(LoginUser loginUser) {
        loginUser.setLoginTime(System.currentTimeMillis());
        String token = UUID.randomUUID().toString();
        loginUser.setToken(token);
        cacheObj(loginUser);
        String jtwToken = createJwtToken(loginUser);
        return new Token(jtwToken, loginUser.getLoginTime());
    }

    @Override
    public void refresh(LoginUser loginUser) {
        cacheObj(loginUser);
    }

    @Override
    public LoginUser getUserByToken(String jwtToken) {
        String token = getRedisTokenFromJwtToken(jwtToken);
        if(StringUtils.isEmpty(token)){
            return null;
        }
        Boolean flag = redisTemplate.hasKey(redisKey+token);
        return flag?(LoginUser)redisTemplate.boundValueOps(redisKey+token).get():null;
    }

    @Override
    public void deleteToken(String jwtToken) {
        String token = getRedisTokenFromJwtToken(jwtToken);
        if(StringUtils.isNotEmpty(token)){
            redisTemplate.delete(redisKey+token);
        }
    }

    private void cacheObj(LoginUser loginUser) {
        loginUser.setExpireTime(System.currentTimeMillis() + expireSeconds * 1000);
        redisTemplate.boundValueOps(redisKey + loginUser.getToken()).set(loginUser, expireSeconds, TimeUnit.SECONDS);
    }


    public String createJwtToken(LoginUser loginUser){
        //创建JWS头，设置签名算法和类型
        JWSHeader jwsHeader = new JWSHeader.Builder(JWSAlgorithm.HS256).
                type(JOSEObjectType.JWT)
                .build();
        //将负载信息封装到Payload中
        Payload payload = new Payload(loginUser.getToken());
        //创建JWS对象
        JWSObject jwsObject = new JWSObject(jwsHeader, payload);
        try {
            //创建HMAC签名器
            JWSSigner jwsSigner = new MACSigner(secret);
            //签名
            jwsObject.sign(jwsSigner);
        } catch (JOSEException e) {
            e.printStackTrace();
        }
        return jwsObject.serialize();
    }

    public String getRedisTokenFromJwtToken(String jwtToken) {
        try {
            //从token中解析JWS对象
            JWSObject jwsObject = JWSObject.parse(jwtToken);
            //创建HMAC验证器
            JWSVerifier jwsVerifier = new MACVerifier(secret);
            if (!jwsObject.verify(jwsVerifier)) {
                throw new BadJWTException("token签名不合法！");
            }
            String token = jwsObject.getPayload().toString();
            return token;
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (JOSEException e) {
            e.printStackTrace();
        } catch (BadJWTException e) {
            e.printStackTrace();
        }
        return null;
    }

    public RSAKey getDefaultRSAKey() {
        //从classpath下获取RSA秘钥对
        KeyStoreKeyFactory keyStoreKeyFactory = new KeyStoreKeyFactory(new ClassPathResource("jks/jwt.jks"), "dianay".toCharArray());
        KeyPair keyPair = keyStoreKeyFactory.getKeyPair("jwt", "dianay".toCharArray());
        //获取RSA公钥
        RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();
        //获取RSA私钥
        RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();
        return new RSAKey.Builder(publicKey).privateKey(privateKey).build();
    }
}
