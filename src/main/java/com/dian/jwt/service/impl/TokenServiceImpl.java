package com.dian.jwt.service.impl;

import com.alibaba.fastjson.JSON;
import com.dian.jwt.pojo.Token;
import com.dian.jwt.service.ITokenService;
import com.dian.permission.pojo.LoginUser;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.annotation.Resource;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Created by dian on 2020/8/7 18:15
 */
@Service
public class TokenServiceImpl implements ITokenService {

    @Value("${token.expire.seconds}")
    private Integer expireSeconds;

    @Value("${redis.spring.security.key}")
    private String redisKey;

    @Resource
    private RedisTemplate<String,LoginUser> redisTemplate;


    @Override
    public Token getToken(LoginUser loginUser) {
        loginUser.setLoginTime(System.currentTimeMillis());
        String token = UUID.randomUUID().toString();
        loginUser.setToken(token);
        cacheObj(loginUser);
        return new Token(token,loginUser.getLoginTime());
    }

    @Override
    public void refresh(LoginUser loginUser) {
        cacheObj(loginUser);
    }

    @Override
    public LoginUser getUserByToken(String token) {
        Boolean flag = redisTemplate.hasKey(redisKey+token);
        return flag?(LoginUser)redisTemplate.boundValueOps(redisKey+token).get():null;
    }

    @Override
    public void deleteToken(String token) {
        redisTemplate.delete(redisKey+token);
    }


    private void cacheObj(LoginUser loginUser){
        loginUser.setExpireTime(System.currentTimeMillis()+expireSeconds*1000);
        redisTemplate.boundValueOps(redisKey+loginUser.getToken()).set(loginUser,expireSeconds, TimeUnit.SECONDS);
    }
}
