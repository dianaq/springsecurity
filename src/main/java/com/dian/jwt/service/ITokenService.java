package com.dian.jwt.service;

import com.dian.jwt.pojo.Token;
import com.dian.permission.pojo.LoginUser;

/**
 * Created by dian on 2020/8/7 18:12
 */
public interface ITokenService {

    Token getToken(LoginUser loginUser);

    void refresh(LoginUser loginUser);

    LoginUser getUserByToken(String token);

    void deleteToken(String token);
}
