package com.dian.config;

import com.dian.filter.TokenFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import javax.annotation.Resource;


/**
 * WebSecurityConfigurerAdapter是Spring提供的对安全配置的适配器
 * 使用@EnableWebSecurity来开启Web安全
 */
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class AuthProviderConfig extends WebSecurityConfigurerAdapter {

    @Resource
    AuthenticationSuccessHandler successHandler;

    @Resource
    AuthenticationFailureHandler failureHandler;

    @Resource
    AuthenticationEntryPoint authenticationEntryPoint;

    @Resource
    LogoutSuccessHandler logoutSuccessHandler;

    @Resource
    AuthUserDetailsService customUserDetailsService;

    @Resource
    TokenFilter tokenFilter;

    /**
     * @Author: dian
     * @Date: 2020/8/6 1:06
     * @Description: 密码的认证方式
     */
    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(customUserDetailsService).passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //开启跨域访问支持
        http.csrf().disable();
        //解决不允许显示在iframe的问题
        http.headers().frameOptions().disable();
        //缓存
        http.headers().cacheControl();
        //基于token 关闭session
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        //允许访问的资源
        http.authorizeRequests()
                .antMatchers("/","/login.html","/login").permitAll()
                .anyRequest().authenticated();
        //登录
        http.formLogin()
                .loginPage("/login.html")
                .loginProcessingUrl("/login")
                .usernameParameter("username")
                .passwordParameter("password")
                .successHandler(successHandler)
                .failureHandler(failureHandler)
                .and()
                .exceptionHandling()
                .authenticationEntryPoint(authenticationEntryPoint);
        //登出
        http.logout().logoutUrl("/logout").logoutSuccessHandler(logoutSuccessHandler);
        //token
        http.addFilterBefore(tokenFilter, UsernamePasswordAuthenticationFilter.class);
    }


    /**
     * 重写configure方法来满足我们自己的需求
     * 此处允许Basic登录
     * @param http
     * @throws Exception
     */
    /*@Override
    protected void configure(HttpSecurity http) throws Exception {
        http.httpBasic() //允许Basic登录
                .and()
                .authorizeRequests() //对请求进行授权
                .anyRequest()  //任何请求
                .authenticated();   //都需要身份认证
    }*/

    /**
     * @Author: dian
     * @Date: 2020/8/6 1:07
     * @Description: 基于form表单的方式
     */
    /*@Override
    protected void configure(HttpSecurity http) throws Exception {
        http.formLogin()
                .loginPage("/login.html")
                .loginProcessingUrl("/login")
                .usernameParameter("username")
                .passwordParameter("password")
                .defaultSuccessUrl("/index")
                .failureUrl("/login.html")
                .and()
                .authorizeRequests() //对请求进行授权
                .antMatchers("/login.html","/login")
                .permitAll()
                .antMatchers("/order")
                .hasAnyAuthority("ROLE_normal","ROLE_admin")
                .antMatchers("/system/user","/system/role","/system/menu")
                // .hasAnyAuthority("ROLE_admin")//
                .hasAnyRole("admin")
                .anyRequest()  //任何请求
                .authenticated()  //都需要身份认证
                .and()
                .csrf().disable();
        http.logout().logoutUrl("/logout");
    }*/

    /**
     * @Author: dian
     * @Date: 2020/8/5 16:38
     * @Description: 基于内存的方式添加用户和密码和角色
     */
    /*@Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("root")
                .password(passwordEncoder().encode("root"))
                .roles("admin");

        auth.inMemoryAuthentication()
                .withUser("dian")
                .password(passwordEncoder().encode("dian"))
                .roles("normal");
        *//*auth.authenticationProvider(kerberosAuthenticationProvider());*//*
    }*/

    /**
     * @Author: dian
     * @Date: 2020/8/6 1:07
     * @Description: 基于ajax请求的方式
     * 相对于form表单 主要是重写了successHandler 和 failureHandler
     */
    /*@Override
    protected void configure(HttpSecurity http) throws Exception {
        http.formLogin()
                .loginPage("/login.html")
                .loginProcessingUrl("/login")
                .usernameParameter("username")
                .passwordParameter("password")
                .successHandler(successHandler)
                .failureHandler(failureHandler)
                .and()
                .authorizeRequests() //对请求进行授权
                .antMatchers("/login.html","/login")
                .permitAll()
                .antMatchers("/order")
                .hasAnyAuthority("ROLE_normal","ROLE_admin","order")
                .antMatchers("/system/user","/system/role","/system/menu")
                // .hasAnyAuthority("ROLE_admin")//
                .hasAnyRole("admin")
                .anyRequest()  //任何请求
                .authenticated()  //都需要身份认证
                .and()
                .csrf().disable();
        http.logout().logoutUrl("/logout");
    }*/
}
