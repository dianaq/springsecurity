package com.dian.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

/**
 * Created by dian on 2020/8/5 15:12
 */
@Configuration
public class WebConfig extends WebMvcConfigurationSupport {

    /**
     * @Author: dian
     * @Date: 2020/8/11 16:36
     * @Description: 配置跨域访问
     */
    @Override
    protected void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**").allowedMethods("*");
    }

    /** 
     * @Author: dian
     * @Date: 2020/8/5 15:13
     * @Description:  如果我们写的过滤器或者第三方过滤器没有使用依赖注入，即这里不使用@Component注解，该如何使得该过滤器正常使用的
     */
    /*@Bean
    public FilterRegistrationBean timeFilter() {
        //初始化一个过滤器注册器
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        TimeFilter timeFilter = new TimeFilter();
        //将自定义的过滤器或者第三方过滤器注册到过滤器链中
        registrationBean.setFilter(timeFilter);

        List<String> urls = new ArrayList<>();
        urls.add("/*");
        //该过滤器对所有的url起作用，但你也可以配置专门的url进行过滤
        registrationBean.setUrlPatterns(urls);
        return registrationBean;
    }*/
}
