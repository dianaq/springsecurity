package com.dian.config;

import com.dian.permission.pojo.LoginUser;
import com.dian.permission.pojo.UserInfo;
import com.dian.permission.service.IUserAuthorityService;
import com.dian.permission.service.IUserInfoService;
import org.springframework.beans.BeanUtils;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created by dian on 2020/8/5 16:25
 */
@Component
public class AuthUserDetailsService implements UserDetailsService {

    @Resource
    IUserInfoService userInfoService;

    @Resource
    IUserAuthorityService userAuthorityService;

    @Resource
    PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        System.out.println("数据库读取账号信息 用户名: "+userName);

        UserInfo userInfo = userInfoService.getByUserName(userName);
        if(null == userInfo){
            throw  new UsernameNotFoundException("not found");
        }else if(userInfo.getState().equals(UserInfo.State.LOCK.type)){
            throw new LockedException("用户被锁定,请联系管理员");
        }else if(userInfo.getState().equals(UserInfo.State.Disabled.type)){
            throw new DisabledException("用户已被禁止登录");
        }

        LoginUser loginUser = new LoginUser();
        BeanUtils.copyProperties(userInfo,loginUser);
        loginUser.setPermissions(userAuthorityService.getUserAuthorityByUserId(loginUser.getId().toString()));
        return loginUser;
    }

    /** User是继承了UserDetails */
        /*List<GrantedAuthority> list = new ArrayList<>();
        list.add(new SimpleGrantedAuthority("ROLE_"+"admin"));
        User user = new User(userInfo.getUserName(),passwordEncoder.encode(userInfo.getPassWord()),list);*/
}
