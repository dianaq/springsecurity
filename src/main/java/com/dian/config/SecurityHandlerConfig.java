package com.dian.config;

import com.dian.common.pojo.RestResult;
import com.dian.common.utils.ResponseUtil;
import com.dian.filter.TokenFilter;
import com.dian.jwt.pojo.Token;
import com.dian.jwt.service.ITokenService;
import com.dian.permission.pojo.LoginUser;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Author: dian
 * @Date: 2020/8/11 17:41
 * @Description: 登录处理器
 */
@Configuration
public class SecurityHandlerConfig {

	@Resource
	private ITokenService tokenService;

	/**
	 * 登陆成功，返回Token
	 * 
	 * @return
	 */
	@Bean
	public AuthenticationSuccessHandler loginSuccessHandler() {
		return (request, response, authentication) -> {
			LoginUser loginUser = (LoginUser)authentication.getPrincipal();
			Token token =  tokenService.getToken(loginUser);
			RestResult result = new RestResult(200, "登录成功", token);
			ResponseUtil.responseJson(response, HttpStatus.OK.value(), result);
		};
	}

	/**
	 * 登陆失败
	 * 
	 * @return
	 */
	@Bean
	public AuthenticationFailureHandler loginFailureHandler() {
		return (request, response, exception) -> {
			String msg = null;
			if (exception instanceof BadCredentialsException){
				msg = "\"密码错误\"";
			}else {
				msg = exception.getMessage();
			}
			RestResult result = new RestResult(HttpStatus.BAD_REQUEST.value(), msg);
			ResponseUtil.responseJson(response, HttpStatus.BAD_REQUEST.value(), result);
		};

	}

	/**
	 * 未登录，返回401
	 * 
	 * @return
	 */
	@Bean
	public AuthenticationEntryPoint authenticationEntryPoint() {
		return (request, response, authException) -> {
			RestResult result = new RestResult(HttpStatus.UNAUTHORIZED.value(), "请先登录");
			ResponseUtil.responseJson(response, HttpStatus.UNAUTHORIZED.value(), result);
		};
	}

	/**
	 * 退出处理
	 * 
	 * @return
	 */
	@Bean
	public LogoutSuccessHandler logoutSussHandler() {
		return (request, response, authentication) -> {
			String token = TokenFilter.getToken(request);
			tokenService.deleteToken(token);
			RestResult result = new RestResult(HttpStatus.OK.value(), "退出成功");
			ResponseUtil.responseJson(response, HttpStatus.OK.value(), result);
		};
	}

}
