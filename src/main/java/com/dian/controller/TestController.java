package com.dian.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * Created by dian on 2020/8/5 23:54
 */
@RestController
public class TestController {

    @GetMapping("/test")
    public String test(){
        return "hello";
    }

}
