package com.dian.controller;

import com.dian.common.utils.AccountHelper;
import com.dian.permission.pojo.LoginUser;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 三种获取当前用户的方法
 * Created by dian on 2020/7/31 10:15
 */
@RestController
public class UserController {

    @GetMapping("/user")
    public LoginUser user() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        LoginUser loginUser =(LoginUser) authentication.getPrincipal();
        return loginUser;
    }

    @GetMapping("/getUser")
    public LoginUser getUser(@AuthenticationPrincipal LoginUser loginUser) {
        return loginUser;
    }

    @GetMapping("/userMsg")
    public LoginUser userMsg(Authentication authentication) {
        LoginUser loginUser =(LoginUser) authentication.getPrincipal();
        return loginUser;
    }

    @GetMapping("/userMsgByUtil")
    public LoginUser userMsgByUtil() {
        return AccountHelper.getLoginUser();
    }


}
